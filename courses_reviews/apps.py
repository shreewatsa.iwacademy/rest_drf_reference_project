from django.apps import AppConfig


class CoursesReviewsConfig(AppConfig):
    name = 'courses_reviews'
