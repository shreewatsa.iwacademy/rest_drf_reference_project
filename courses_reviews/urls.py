from django.urls import path, include
from rest_framework import routers

from . import views

app_name = "courses"

router = routers.SimpleRouter()
router.register('courses', views.CourseViewSet)
router.register('reviews', views.ReviewViewSet)
# urlpatterns = router.urls

urlpatterns = [
    # path("v1/courses/", views.ListCreateCourse.as_view(), name="course_list"),
    path("v1/courses/", views.ListCreateCourseGeneric.as_view(), name="course_list"),
    path("v1/courses/<int:pk>/", views.RetriveUpdateDestroyCourse.as_view(), name="course_detail"),
    path("v1/courses/<int:course_pk>/reviews/", views.ListCreateReview.as_view(), name="review_list"),
    path("v1/courses/<int:course_pk>/reviews/<int:pk>", views.RetrieveUpdateDestroyReview.as_view(), name="review_detail"),
    path("v2/", include(router.urls))
]
