from django.shortcuts import get_object_or_404

from rest_framework import generics, viewsets, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import action
from rest_framework import mixins, permissions

from . import models, serializers
from . import permissions as custom_permissions

class ListCreateCourse(APIView):
    def get(self, request, format=None):
        courses = models.Course.objects.all()
        serializer =  serializers.CourseSerializer(courses, many=True)
        return Response(serializer.data)
    
    def post(self, request, format=None):
        serializer = serializers.CourseSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()   #Note:: data is saved to database and serializer is updated ie now it contains id , etc.
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ListCreateCourseGeneric(generics.ListCreateAPIView):
    queryset = models.Course.objects.all()
    serializer_class = serializers.CourseSerializer


class RetriveUpdateDestroyCourse(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Course.objects.all()
    serializer_class = serializers.CourseSerializer


class ListCreateReview(generics.ListCreateAPIView):
    queryset = models.Review.objects.all()
    serializer_class = serializers.ReviewSerializer
    
    def get_queryset(self):
        return self.queryset.filter(course_id = self.kwargs.get('course_pk'))

    def perform_create(self, serializer):
        course = get_object_or_404(models.Course, pk=self.kwargs.get("course_pk"))
        serializer.save(course=course)
        

class RetrieveUpdateDestroyReview(generics.RetrieveUpdateDestroyAPIView):
    queryset = models.Review.objects.all()
    serializer_class = serializers.ReviewSerializer

    def get_object(self):
        return get_object_or_404(
            models.Review,
            course_id = self.kwargs.get("course_pk"),
            pk = self.kwargs.get("pk")
        )


class CourseViewSet(viewsets.ModelViewSet):
    permission_classes = (custom_permissions.IsSuperUser, permissions.DjangoModelPermissions,)
    queryset = models.Course.objects.all()
    serializer_class = serializers.CourseSerializer

    @action(methods=['GET'], detail = True)
    def reviews(self, request, pk=None  ):
        # course = self.get_object()
        # serializer = serializers.ReviewSerializer(course.reviews.all(), many=True)
        # return Response(serializer.data)

        #customizing pagination
        self.pagination_class.page_size = 2
        reviews = models.Review.objects.filter(course_id=pk)
        page = self.paginate_queryset(reviews)
        if page is not None:
            serializer = serializers.ReviewSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = ReviewViewSet(reviews, many=True)
        return Response(serializer.data)



class ReviewViewSet(mixins.CreateModelMixin,
                    mixins.DestroyModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    viewsets.GenericViewSet):
    queryset = models.Review.objects.all()
    serializer_class = serializers.ReviewSerializer