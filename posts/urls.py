from django.urls import path, include

from . import views

urlpatterns = [
    path("<int:pk>/", views.PostDetailView.as_view(), name="post_detail"),
    path("", views.PostListView.as_view(), name="post_list"),
]