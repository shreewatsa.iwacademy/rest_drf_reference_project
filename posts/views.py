from rest_framework import status
from rest_framework import generics, permissions

from .models import Post
from .serializers import PostSerializer
from .permissions import IsAuthorOrReadOnly


# class PostListView(generics.ListAPIView):                 # Read only List view
class PostListView(generics.ListCreateAPIView):             # Read and Create List view
    # permission_classes = (permissions.IsAuthenticated,)   #view-level permissions
    queryset = Post.objects.all()
    serializer_class = PostSerializer


# class PostDetailView(generics.RetrieveAPIView):               # Read only detail view
class PostDetailView(generics.RetrieveUpdateDestroyAPIView):    # Read, Deltete and Update detail view
    # permission_classes = (permissions.IsAuthenticated,)       #view-level permissions
    permission_classes = (IsAuthorOrReadOnly,)
    queryset = Post.objects.all()
    serializer_class = PostSerializer
