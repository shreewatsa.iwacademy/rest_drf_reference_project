from django.urls import path, include

urlpatterns = [
    path('api/v1/posts/', include("posts.urls")),
    path('api/', include("serializerapp.urls")),
    path('api/', include("courses_reviews.urls", namespace="courses")),
]
