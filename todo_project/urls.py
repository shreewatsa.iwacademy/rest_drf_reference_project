
from django.contrib import admin
from django.urls import path, include
from rest_framework.authtoken import views


# Schema and documentation

API_TITLE = "Todo Projects APIs"
API_DESCRIPTION = "Rest API in DRF 101"
from rest_framework.documentation import include_docs_urls
from rest_framework.schemas import get_schema_view
from rest_framework_swagger.views import get_swagger_view
# schema_view = get_schema_view(title=API_TITLE, description=API_DESCRIPTION)
swagger_schema_view = get_swagger_view(title=API_TITLE)

# end of schema and documentation


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include("api.urls")),
    path('api-token-auth/', views.obtain_auth_token),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),  # To be able to login/logout in browsable api.
    path('api/v1/rest-auth/', include('rest_auth.urls')),                           # login/logout/password reset/confirm,etc via api.
    path('api/v1/rest-auth/registration/', include('rest_auth.registration.urls')), # user registration via api, social auth, etc.
    # path('schema/', schema_view),                                                 # Url to view schema, but not human friendly.
    path('docs/', include_docs_urls(title=API_TITLE, description=API_DESCRIPTION)), # Human friendly documentation of apis.
    path('swagger-docs/', swagger_schema_view),                                     # Url to view schema, but not human friendly.
]
