from django.contrib.auth import get_user_model
from rest_framework import serializers

User = get_user_model()
from .models import Student

class UserSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=10)
    email = serializers.EmailField()
    password = serializers.CharField(max_length=10)

class UserModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('id', 'username')
    
class StudentModelSerializer(serializers.ModelSerializer):
    # user = UserModelSerializer()
    # user = serializers.HyperlinkedRelatedField(many=False, read_only=True, view_name="users_urls:users")
    # user = serializers.PrimaryKeyRelatedField(many=False, read_only=True)
    class Meta:
        model = Student
        fields = "__all__"

    # def create(self, validated_data):
    #     breakpoint()
    #     username = validated_data.pop("user")
    #     user = User.objects.get(username=username)
    #     student = Student.objects.create(user=user, **validated_data)
    #     return student

    # def validate_user(self, value):
    #     if User.objects.get(pk=value):
    #         return value
    #     raise serializers.ValidationError("User does not exist !")

    # def update(self, instance, validated_data):
    #     return super().update(instance, validated_data)

    def to_representation(self, instance):
        res = super().to_representation(instance)
        user = UserModelSerializer(instance.user).data
        res['user'] = user
        return res

class StudentReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = "__all__"
