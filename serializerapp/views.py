import csv
from django.shortcuts import render
from django.contrib.auth import get_user_model

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, generics, viewsets
from rest_framework.renderers import JSONRenderer
from rest_framework.permissions import IsAuthenticated, AllowAny

from .serializers import UserSerializer, UserModelSerializer, StudentModelSerializer, StudentReadSerializer
from . import csv_read_write
from .models import Student
from . import permissions as custom_permission

User = get_user_model()
# Create your views here.

class UserListView(APIView):
    def get(self, request, format=None):
        users = User.objects.all()
        serialized_users = UserSerializer(users, many=True)
        
        ## Working with csv
        # users_to_be = csv_read_write.get_users()
        # print(list(map(lambda user : User.objects.create_user(*user), users_to_be)))
        # csv_read_write.write_to_csv(serialized_users.data)      # write user records to csv
        ## End working with csv
        
        return Response(serialized_users.data)

    def post(self, request, format=None):
        try:
            password = request.data.pop("password")
            serialized_user = UserSerializer(data=request.data)
            if serialized_user.is_valid():
                validated_data = serialized_user.validated_data
                new_user = User.objects.create(**validated_data)
                new_user.set_password(password)
                new_user.save()
                return Response(status=status.HTTP_201_CREATED)
        except Exception as e:
            print(e)
        return Response(status=status.HTTP_400_BAD_REQUEST)

class UsersList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserModelSerializer

class UsersDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserModelSerializer

class UserViewSet(viewsets.ModelViewSet):
    # permission_classes = [custom_permission.IsAdminUser,]
    queryset = User.objects.all()
    serializer_class = UserModelSerializer

    def get_permissions(self):
        permission_classes = []
        if self.action == "list":
            permission_classes.append(AllowAny)
        elif self.action == "retrieve":
            permission_classes.append(custom_permission.IsAdminUser)
            # permission_classes.append(IsAuthenticated)

        return [permission() for permission in permission_classes]

class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentModelSerializer

    # def create(self, request, *args, **kwargs):
    #     user_id = request.data.pop("user")
    #     user = User.objects.get(id=user_id)

    #     return super().create(request, *args, **kwargs)

    # def get_serializer_class(self, *args, **kwargs):
    #     if self.action == "list":
    #         return StudentReadSerializer
    #     return StudentModelSerializer
        
    # def list(self, request, *args, **kwargs):
    #     res = self.get_serializer(self.get_queryset, many=True).data
    #     new_res = {
    #         "data":res,
    #         "message": "Retrieve list was successful"
    #     }
    #     return Response(new_res, status=status.HTTP_200_OK)

    