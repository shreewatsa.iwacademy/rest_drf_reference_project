import random
import csv

def write_to_csv(serialized_users=None):
    if not serialized_users:
        return None
    with open("users_records.csv", mode="w") as csv_file:
        fields = ["username", "email", "password"]
        writer = csv.DictWriter(csv_file, fieldnames=fields)
        writer.writeheader()
        # users_data = JSONRenderer().render(serialized_users.data)   
        for user in serialized_users:
            writer.writerow(user)


## Generate users from data.txt ##


mail_types = ["gmail","outlook","hotmail","yahoo"]

users_to_be = []

def create_user_credentials(username=None):
    if not username:
        return None
    user = [username, f'{username}@{random.choice(mail_types)}.com', f'{username}{random.randint(1,10)}']
    users_to_be.append(user)

def get_users():
    with open("/home/shreewatsa/Documents/IWacademy/DjangoIWAcademy/Assignments/TODO_REST_API/Csv_Excel/data.txt", "r") as file:
        for line in file.readlines():
            rowList = line.split()
            names = rowList[1::2] 
            create_user_credentials(names[0])
            create_user_credentials(names[1])
    # return users_to_be 
    return None

########################################################################################################
# CSV practice

# with open('employee_birthday.txt', mode='r') as csv_file:
    # csv_reader = csv.DictReader(csv_file)
    # csv_reader = csv.reader(csv_file, delimiter=',')
    #     for row in csv_reader: 
    #         continue

# with open('employee_file.csv', mode='w') as employee_file:  
    # employee_writer = csv.writer(employee_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    # employee_writer.writerow(['John Smith', 'Accounting', 'November'])
    
# If quoting is set to csv.QUOTE_MINIMAL, then .writerow() will quote fields only if they contain the delimiter or the quotechar. This is the default case.
# If quoting is set to csv.QUOTE_ALL, then .writerow() will quote all fields.
# If quoting is set to csv.QUOTE_NONNUMERIC, then .writerow() will quote all fields containing text data and convert all numeric fields to the float data type.
# If quoting is set to csv.QUOTE_NONE, then .writerow() will escape delimiters instead of quoting them. In this case, you also must provide a value for the escapechar optional parameter.

# with open('employee_file.csv', mode='w') as employee_file:  
    # fieldnames = ['emp_name', 'dept', 'birth_month']
    # writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

    # writer.writeheader()
    # writer.writerow({'emp_name': 'John Smith', 'dept': 'Accounting', 'birth_month': 'November'})

# delimiter, quotechar, escapechar, quoting