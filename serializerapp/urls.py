from django.urls import path, include

from . import views

# Using router and viewsets

from rest_framework.routers import SimpleRouter                         # DefaultRouter
router = SimpleRouter()
router.register('users', views.UserViewSet, basename="users_urls"),
router.register('students', views.StudentViewSet, basename="students_urls")
# urlpatterns = router.urls

# End router and viewsets

app_name = "serializerapp"
urlpatterns = [
    # path("v1/", views.UserListView.as_view(), name="user_list"),         # Normal APIView
    
#     # path('v1/', views.UsersList.as_view()),                            # Generic APIView , ListCreateAPIView
#     # path('v1/<int:pk>/', views.UsersDetail.as_view()),                 # Generic APIView, RetrieveUpdateDestroyAPIView 
    
    
#     # path("v1/all/", views.UserViewSet.as_view({'get':'list'})),                # Using modelviewset without router.
#     # path("v1/all/<int:pk>/", views.UserViewSet.as_view({'get':'retrieve'})),   # Using modelviewset without router.
    path("v2/", include(router.urls), name="apiv2")
]