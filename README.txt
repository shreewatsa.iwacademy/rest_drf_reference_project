What's in here ?
1) Working with APIView
2) CRUD with Generic Views (ListCreateAPIView, RetrieveUpdateDestroyAPIView)
3) Working with ModelViewSet and routers
4) Serializers, custom validations, customizing serialization
5) Throttling
6) Authorization, Custom Permissions , Permissions(Project level, View level, Object level)
7) Authentication (TokenAuthentication)
8) Pagination
9) Schema and Documentation of API (CoreApi, django-rest-swagger)